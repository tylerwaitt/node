var DB_URL = 'mongodb://localhost/coffee-box-db';
var async = require('async');
var mongoose = require('mongoose');
var Faker = require('Faker');
var Post = require('../app/models/post').Post;

mongoose.connect(DB_URL);

populatePost = function(index, callback) {
  var comment, comments, j, numComments, post, _i;
  post = {};
  post.title = Faker.Lorem.sentence();
  post.slug = Faker.Lorem.words(3).join('-');
  post.rawContent = Faker.Lorem.paragraphs(4);
  post.asPage = index > 17;
  if (post.asPage) {
    post.title = post.title.slice(0, 5);
  }
  comments = [];
  numComments = Math.round(Math.random() * 5);
  for (j = _i = 1; 1 <= numComments ? _i <= numComments : _i >= numComments; j = 1 <= numComments ? ++_i : --_i) {
    comment = {};
    comment.name = Faker.Name.firstName();
    comment.email = Faker.Internet.email();
    comment.website = Faker.Internet.domainName();
    comment.rawContent = Faker.Lorem.paragraphs(2);
    comment.spam = Math.random() > 0.7;
    comment.read = false;
    comments.push(comment);
  }
  post.comments = comments;
  return (new Post(post)).save(function(err) {
    return callback(err);
  });
};

async.forEach([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], populatePost, function(err) {
  if (err) {
    console.log(err);
  }
  mongoose.disconnect();
  return process.exit();
});