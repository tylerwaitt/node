var mongoose = require("mongoose");
var validate = require('mongoose-validator');

var shoutValidator = [
  validate({
    validator: 'isLength',
    arguments: [3, 15],
    message: 'Shout should be between 3 and 15 characters'
  }),
  validate({
    validator: 'isAlphanumeric',
    passIfEmpty: true,
    message: 'Name should contain alpha-numeric characters only'
  })
];

var ShoutSchema = mongoose.Schema({
  name: {
    type: String,
    "default": '',
    trim: true
  },
  shout: {type: String, "default": '', trim: true, validate: shoutValidator}
});

ShoutSchema.methods.upper = function() {
  return this.name.toUpperCase();
};

module.exports = mongoose.model("Shout", ShoutSchema);
