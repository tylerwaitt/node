var mongoose = require('mongoose');
var User = mongoose.model('User');

exports.account = function(req, res) {
  return User.findOne({
    _id: req.user.id
  }).exec(function(err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next(new Error('Failed to load User ' + id));
    }
    return res.render('settings/account', {
      title: user.name,
      user: user
    });
  });
};

exports.password = function(req, res) {
  return res.render('settings/password');
};

exports.notifications = function(req, res) {
  return res.render('settings/notifications');
};

exports.profile = function(req, res) {
  return User.findOne({
    _id: req.user.id
  }).exec(function(err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next(new Error('Failed to load User ' + id));
    }
    return res.render('settings/profile', {
      title: user.name,
      user: user
    });
  });
};