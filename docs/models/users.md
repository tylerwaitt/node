# User model
check the schema to get the most updated look

## validations
- username
  * min: 3
  * max: 15
  * no whitespace
  * no special chars
  * hyphen and underscore
  * must be unique
  * trim
  - cant start with hyphen or number
  - no hyphens in a row
- fullName
  * min: 3
  * max: 20
  * no special chars
  - hyphen allowed
  - no numbers
  * trim
- email
  * valid email address
  * can't be blank
  * must be unique
  * 250 char limit
  * trim
- password
  - can't be blank
