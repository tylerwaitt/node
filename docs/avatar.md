# avatars

Whatever the user signs up with first (LOCAL or SOCIAL) will determine what their starting avatar is.


If avatar = 'blank', gravatar with default of random avatar.

- LOCAL
  - GRAVATAR?
    - YES: use it
    - NO: randomAvatar()

- SOCIAL
  - FACEBOOK?
    - do they allow pulling avatars?
  - GOOGLE?
    - do they allow pulling avatars?
