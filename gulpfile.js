var gulp = require('gulp');
var axis = require('axis');
var jeet = require('jeet');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var minifycss = require('gulp-minify-css');
var stylus = require('gulp-stylus');
var nodemon = require('gulp-nodemon');
var rename = require('gulp-rename');
var livereload = require('gulp-livereload');

gulp.task('stylus', function() {
  return gulp.src('public/styl/main.styl').pipe(plumber()).pipe(stylus()).pipe(gulp.dest('public/dist')).pipe(rename({
    suffix: '.min'
  })).pipe(minifycss()).pipe(gulp.dest('public/dist')).pipe(livereload());
});

gulp.task('jade', function() {
  return gulp.src('app/views/**/*.jade').pipe(livereload());
});

gulp.task('server', function() {
  return nodemon({
    script: 'server.js'
  });
});

gulp.task('watch', function() {
  gulp.watch('public/styl/**/*.styl', ['stylus']);
  return gulp.watch('app/views/**/*.jade', ['jade']);
});

gulp.task('default', ['server', 'stylus', 'jade', 'watch']);