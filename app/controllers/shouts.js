var mongoose = require('mongoose');
var Shout = mongoose.model('Shout');

exports.index = function(req, res) {
  return Shout.find(function(err, shouts) {
    var red, _i, _len;
    for (_i = 0, _len = shouts.length; _i < _len; _i++) {
      red = shouts[_i];
      console.log(red.upper());
    }
    if (err) {
      console.error(err);
    }
    if (shouts) {
      console.dir("WE FOUND STUFF");
      console.dir(shouts);
      return res.render('shouts/index', {
        shouts: shouts
      });
    }
  });
};

exports.shout = function(req, res) {
  var name, shout;
  if (req.body.name) {
    name = req.body.name;
  } else {
    name = "Anonymous";
  }
  if (req.body.shout) {
    shout = req.body.shout;
  } else {
    shout = "No msg";
  }
  shout = new Shout({
    name: name,
    shout: shout
  });
  shout.save(function(err, shout) {
    if (err) {
      console.error(err);
    }
    return console.dir(shout);
  });
  return res.redirect('/shouts');
};