var mongoose = require('mongoose');
var User = mongoose.model('User');
var async = require('async');
var randtoken = require('rand-token');
var nodemailer = require('nodemailer');
var gravatar = require('gravatar');
var easyimg = require('easyimage');


exports.login = function(req, res) {
  return res.render('auth/login', {
    user: req.user
  });
};

exports.show = function(req, res) {
  User.findOne({_id: req.params.userId}).exec(function(err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next(new Error('Failed to load User ' + id));
    }
    var avatar = gravatar.url(user.email, {s: '100', d: 'https://s3.amazonaws.com/iloveu/global_avatars/01-100.jpg'});

    return res.render('users/show', {
      title: user.name,
      user: user,
      avatar: avatar
    });
  });
};

exports.authCallback = function (req, res, next) {
  res.redirect('/');
};

exports.join = function(req, res) {
  return res.render('auth/join',{
    user: new User()
  });
};

exports.index = function(req, res) {
  return User.find(function(err, users) {
    if (err) {
      console.error(err);
    }
    if (users) {
      return res.render('users/index', {
        users: users
      });
    }
  });
};

exports.session = function(req, res) {
  return res.redirect('/');
};

exports.logout = function(req, res) {
  req.logout();
  return res.redirect('/login');
};

exports.create = function(req, res) {
  var newUser;
  newUser = new User(req.body);
  //console.dir(newUser);
  User.findOne({email: newUser.email}).exec(function(err, user) {
    if (err) {
      next(err);
    }
    if (!user) {
      newUser.save(function(err) {
        if (err) {
          console.log(err.errors);
          console.log(newUser);
          req.flash('errors', err.errors);
          req.flash('user', newUser);
          res.render('auth/join', {errors: err.errors,user: newUser});
          //return res.redirect('/join');
        }else{
          req.login(newUser, function(err) {
            if (err) {
              console.log(err);
              next(err);
            }
            req.flash('info', 'Welcome to the show');
            return res.redirect('/');
          });
        }
      });
    } else {
      console.log("USER BE THERE");
      return res.render('auth/join', {
        errors: [
          {
            "message": "email already registered"
          }
        ],
        user: newUser
      });
    }
  });
};

exports.forgotGet = function(req, res) {
  return res.render('auth/forgot');
};



exports.forgotPost = function(req, res) {

  User.findOne({ email: req.body.email }, function(err, user) {
    if (!user) {
      req.flash("error", "That email be not in the system yo.");
      return res.redirect('/forgot');
    }

    var token = randtoken.generate(16);
    user.resetPasswordToken = token;
    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

    user.save(function(err) {
      console.log('saved user with token variable');
      var transporter = nodemailer.createTransport({
        service: 'Mailgun',
        auth: {
            user: process.env.MAIL_KEY,
            pass: process.env.MAIL_SECRET
        }
      });

      var mailOptions = {
        to: user.email,
        from: process.env.MAIL_FROM,
        subject: 'Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };

      transporter.sendMail(mailOptions, function(err) {
        req.flash('info', 'We sent you an email with a link to reset your password.  Go check!');
        return res.redirect('/');
      });
    });
  });
};

exports.resetGet = function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
    }
    res.render('auth/reset', {
      user: req.user
    });
  });
};

exports.resetPost = function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('back');
    }

    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpires = undefined;

    user.save(function(err) {
      req.logIn(user, function(err) {
        req.flash('info', 'Success! Your password has been changed.');
        return res.redirect('/');
      });
    });
  });
};
