# Welcome
This is my node starter project.  It will contain:

- User system
  - passport
    - local
    - facebook
    - google+
  - forgot password
  - avatar uploads
    - local
      - gravatar
      - random default
    - social auth avatar?
    - upload own avatar to s3
- Shoutbox


## dotenv
We use dotenv for env setup.  For local development, simply place a .env file in project root.  There's a sample at .env.sample.

## uploads folder
For avatar uploads to s3, we upload them to the 'uploads' folder first to resize the images.  This folder may need to be created as it's in .gitignore.
