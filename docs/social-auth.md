Join strategies.

Local first, then Facebook.
facebook.profile.id attached to any user?
NO - check to see if the facebook.profile.email exists in the database.
  YES - 
    EITHER
      GRANT ACCESS and save facebook.profile
      DENY ACCESS and warn that email address is already in use
  NO - create user

Facebook first, then Local.
email address in the database?
YES
  local account?
    YES - GIVE ERROR
    NO - Means already joined with social
      EITHER
        - GRANT ACCESS and save password
        - DENY ACCESS and warn that email address is already in use



SOCIAL - LOCAL
Not allowed

SOCIAL - SOCIAL - LOCAL
Not allowed

LOCAL - SOCIAL
Allowed

LOCAL - SOCIAL - SOCIAL
Allowed

SOCIAL - SOCIAL
Allowed