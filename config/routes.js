module.exports = function(app, passport, req) {
  var auth = require('../config/middlewares/auth');
  var pages = require('../app/controllers/pages');
  var shouts = require('../app/controllers/shouts');
  var users = require('../app/controllers/users');
  var settings = require('../app/controllers/settings');
  var avatar = require('../app/controllers/avatar');

  app.get('/', pages.home);
  app.get('/faq', pages.faq);

  app.get('/shouts', shouts.index);
  app.post('/shout', shouts.shout);

  app.get('/settings/account', auth.requiresLogin, settings.account);
  app.get('/settings/password', settings.password);
  app.get('/settings/notifications', settings.notifications);
  app.get('/settings/profile', settings.profile);

  app.get('/users', users.index);
  app.get('/users/:userId', users.show);

  app.post('/upload', avatar.upload);

  app.get('/forgot', users.forgotGet);
  app.post('/forgot', users.forgotPost);
  app.get('/reset/:token', users.resetGet);
  app.post('/reset/:token', users.resetPost);

  app.get('/login', users.login);
  app.get('/logout', users.logout);
  app.get('/join', users.join);
  app.post('/join', users.create);
  app.post("/login", passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true
  }));

  app.get('/auth/facebook', passport.authenticate('facebook', { scope: [ 'email', 'user_about_me'], failureRedirect: '/login' }), users.join);
  app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }), users.authCallback);

  app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'] }));
  app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/login', successRedirect: '/' }));

};
