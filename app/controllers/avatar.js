var fs = require('fs-extra');
var busboy = require('connect-busboy');
var easyimg = require('easyimage');
var randtoken = require('rand-token');
var AWS = require('aws-sdk');
var env = process.env;

exports.upload = function(req, res, next) {

  var filename = randtoken.generate(16);
  var localPath = __dirname + '/../../uploads/';
  var s3Path = env.SITE_ID + '/avatars/';

  req.pipe(req.busboy);

  req.busboy.on('file', function(fieldname, file) {
    var fstream = fs.createWriteStream(localPath + filename);
    file.pipe(fstream);
    fstream.on('close', function() {
      console.log("Local upload finished for " + filename);
      //res.redirect('back');
    });
  });




  easyimg.thumbnail({
    src: localPath + filename,
    dst: localPath + filename + '-c100' + '.jpg',
    width: 100,
    height: 100
  }).then(
    function(image) {
      console.log('Resized and cropped: ' + image.width + ' x ' + image.height);
    },
    function(err) {
      console.log(err);
    }
  );

  AWS.config.update({
    accessKeyId: env.AWS_KEY,
    secretAccessKey: env.AWS_SECRET
  });
  var s3 = new AWS.S3();
  var bucketName = env.S3_BUCKET;
  var keyName = s3Path + filename;
  var params = {
    Bucket: bucketName,
    Key: keyName,
    Body: 'Hello World!'
  };
  s3.putObject(params, function(err, data) {
    if (err)
      console.log(err);
    else
      console.log("Successfully uploaded data to " + bucketName + "/" + keyName);
  });

  res.redirect('back');

};
