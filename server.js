// REQUIRE
var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var errorhandler = require('errorhandler');
var fs = require('fs-extra');
var busboy = require('connect-busboy');
var dotenv = require('dotenv');


// Load ENV variables
dotenv.load();

// APP
var app = express();

app.use(busboy());

// DB config
var configDB = require('./config/database');

//app.use(errorhandler({ dumpExceptions: true, showStack: true }));
app.use(errorhandler());

// DB CONNECTION
mongoose.connect(configDB.url);

// MODELS
var models_path = __dirname + '/app/models';
fs.readdirSync(models_path).forEach(function(file) {
  return require(models_path + '/' + file);
});

// PASSPORT
require('./config/passport')(passport);

// VIEW ENGINE
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'jade');
app.locals.basedir = __dirname + '/app/views';

// COOKIEPARSER
// app.use(cookieParser());
app.use(cookieParser('there can only be one highlander'));
//app.use(express.session({ cookie: { maxAge: 60000 }}));



// BODYPARSER
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

console.log('your mom');



// SESSIONS
app.use(session({
  secret: 'every passing minute is another chance to turn it all around',
  saveUninitialized: true,
  resave: true,
  cookie: { maxAge: 60000000 }
}));

// FLASH
app.use(flash());

// REQ IN VIEWS
app.use(function(req, res, next) {
  res.locals.session = req.session;
  res.locals.req = req;
  res.locals.message = req.flash('info') + req.flash('error');
  return next();
});

// PASSPORT
app.use(passport.initialize());
app.use(passport.session());

// FAVICON (uncomment after placing your favicon in /public)
//app.use(favicon(__dirname + '/public/favicon.ico'))

// NOT SURE
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/flash', function(req, res) {
  req.flash('info', 'We got a mother trucking flash!');
  return res.redirect('/');
});




// ROUTES
require('./config/routes')(app, passport);





// 404 CATCH (and forward to error handler)
app.use(function(req, res, next) {
  var err;
  err = new Error("Not Found");
  err.status = 404;
  return next(err);
});

// ERROR HANDLER (development - print stacktrace)
if (app.get("env") === "development") {
  app.locals.pretty = true;
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    return res.render("error", {
      message: err.message,
      error: err
    });
  });
}

// ERROR HANDLER (production - no stacktraces leaked to user)
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  return res.render("error", {
    message: err.message,
    error: {}
  });
});

// START LISTENING
app.set("port", process.env.PORT || 3000);
app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'));
});


// EXPOSE
module.exports = app;
