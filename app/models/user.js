var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var validate = require('mongoose-validator');


var usernameValidator = [
  validate({
    validator: 'isLength',
    arguments: [3, 15],
    passIfEmpty: true,
    message: 'Username should be between 3 and 15 characters'
  }),
  validate({
    validator: 'matches',
    arguments: (/^[a-z0-9-]+$/i),
    passIfEmpty: true,
    message: 'Username should contain alpha-numeric characters only and hyphen'
  })
];

var fullNameValidator = [
  validate({
    validator: 'isLength',
    arguments: [3, 20],
    message: 'Full name should be between 3 and 20 characters'
  }),
  validate({
    validator: 'matches',
    arguments: (/^[a-z- ]+$/i),
    passIfEmpty: true,
    message: 'Full name should contain alpha characters only'
  })
];

var emailValidator = [
  validate({
    validator: 'isLength',
    arguments: [3, 250],
    message: "Email can't be longer than 250 chars"
  }),
  validate({
    validator: 'isEmail',
    message: "Email not valid"
  })
];

var passwordValidator = [
  validate({
    validator: 'isLength',
    arguments: [3, 250],
    message: "Password not long enough"
  })
];

var userSchema = mongoose.Schema({
  email: {type: String, required: 'Need email yo', validate: emailValidator, unique: true, default: ''},
  username: {type: String, unique: true, validate: usernameValidator, default: ''},
  fullName: {type: String, required: 'Need a full name', validate: fullNameValidator, unique: true, default: ''},
  hashed_password: {type: String, required: "Password can't be blank", validate: passwordValidator},
  provider: String,
  salt: String,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  facebook: {},
  twitter: {},
  github: {},
  google: {}
});

userSchema.virtual('password').set(function(password) {
  this._password = password;
  this.hashed_password = this.encryptPassword(password);
}).get(function() {
  return this._password;
});

userSchema.methods.encryptPassword = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.authenticate = function(plainText) {
  return bcrypt.compareSync(plainText, this.hashed_password);
};

module.exports = mongoose.model("User", userSchema);
